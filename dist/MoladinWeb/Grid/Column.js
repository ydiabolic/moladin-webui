"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Column = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

require("./grid.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Column = function Column(p) {
  var mobile = p.mobile || 'col';
  var desktop = p.desktop || '';
  var className = p.className || '';
  return /*#__PURE__*/_react.default.createElement("div", {
    className: [mobile, desktop, className].join(' ')
  }, p.children);
};

exports.Column = Column;
Column.propTypes = {
  className: _propTypes.default.string,
  mobile: _propTypes.default.oneOf(['col', 'col-2', 'col-2-5', 'col-3', 'col-4', 'col-5', 'col-6', 'col-7', 'col-8', 'col-10', 'col-12', 'col-auto']),
  desktop: _propTypes.default.oneOf(['col-md', 'col-md-2', 'col-md-2-5', 'col-md-3', 'col-md-4', 'col-md-5', 'col-md-6', 'col-md-7', 'col-md-8', 'col-md-10', 'col-md-12', 'col-md-auto'])
};
Column.defaultProps = {
  className: null,
  mobile: null,
  desktop: null
};