"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Grid = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

require("./grid.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Grid = function Grid(p) {
  var alignItems = '';

  switch (p.alignItems) {
    case 'center':
      alignItems = 'justify-content-center';
      break;

    case 'right':
      alignItems = 'justify-content-end';
      break;

    default:
      break;
  }

  return /*#__PURE__*/_react.default.createElement("div", {
    className: ['mol-row', alignItems].join(' ')
  }, p.children);
};

exports.Grid = Grid;
Grid.propTypes = {
  alignItems: _propTypes.default.oneOf(['left', 'center', 'right'])
};
Grid.defaultProps = {
  alignItems: 'left'
};