"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.usageColumn = exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _Column = require("./Column");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = {
  title: 'MoladinWeb/Grid/Column',
  component: _Column.Column
};
exports.default = _default;

var TemplateColumn = function TemplateColumn(args) {
  return /*#__PURE__*/_react.default.createElement(_Column.Column, args);
};

var usageColumn = TemplateColumn.bind({});
exports.usageColumn = usageColumn;
usageColumn.args = {
  mobile: 'col',
  children: 'Test column'
};