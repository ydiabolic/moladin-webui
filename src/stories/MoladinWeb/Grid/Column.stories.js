import React from 'react';
import { Column } from './Column';

export default {
    title: 'MoladinWeb/Grid/Column',
    component: Column
};

const TemplateColumn = (args) => <Column {...args} />;

export const usageColumn = TemplateColumn.bind({});
usageColumn.args = {
    mobile: 'col',
    children: 'Test column'
};