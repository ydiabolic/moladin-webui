import React from 'react';
import { Grid } from './index';
import { Column } from './Column';

export default {
  title: 'MoladinWeb/Grid',
  component: Grid
};

const TemplateGrid = ({columns, ...args}) => {
  return (
    <Grid {...args}>
      {(columns || []).map((c, i) => {
        return (
          <Column mobile={c.mobile} key={i}>
            {c.body}
          </Column>
        );
      })}
    </Grid>
  );
};

export const usage = TemplateGrid.bind({});
usage.args = {
  alignItems: 'left',
  columns: [
    {
      body: 'Col 1',
      mobile: null
    },
    {
      body: 'Col 2',
      mobile: null
    }
  ]
};