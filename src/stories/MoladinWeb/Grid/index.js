import React from 'react';
import PropTypes from 'prop-types';
import './grid.scss';

export const Grid = (p) => {
  let alignItems = '';

  switch (p.alignItems) {
    case 'center':
      alignItems = 'justify-content-center';
      break;
    case 'right':
      alignItems = 'justify-content-end';
      break;
    default:
      break;
  }
  
  return (
    <div className={['mol-row', alignItems].join(' ')}>
      {p.children}
    </div>
  );
};

Grid.propTypes = {
  alignItems: PropTypes.oneOf(['left', 'center', 'right']),
};

Grid.defaultProps = {
  alignItems: 'left'
};
