import React from 'react';
import PropTypes from 'prop-types';
import './grid.scss';

export const Column = (p) => {
    const mobile = p.mobile || 'col';
    const desktop = p.desktop || '';
    const className = p.className || '';

    return (
        <div className={[mobile, desktop, className].join(' ')}>
            {p.children}
        </div>
    );
};

Column.propTypes = {
    className: PropTypes.string,
    mobile: PropTypes.oneOf(['col', 'col-2', 'col-2-5', 'col-3', 'col-4', 'col-5', 'col-6', 'col-7', 'col-8', 'col-10', 'col-12', 'col-auto']),
    desktop: PropTypes.oneOf(['col-md', 'col-md-2', 'col-md-2-5', 'col-md-3', 'col-md-4', 'col-md-5', 'col-md-6', 'col-md-7', 'col-md-8', 'col-md-10', 'col-md-12', 'col-md-auto'])
};

Column.defaultProps = {
    className: null,
    mobile: null,
    desktop: null
};
  