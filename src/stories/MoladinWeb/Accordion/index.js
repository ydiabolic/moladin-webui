import React, { useState } from 'react';
import './accordion.scss';

export const Accordion = (p) => {
  const [ active, setActive ] = useState(null);

  return (
    <div className="mol-accordion">
      {p.data.map((d, i) => {
        return (
          <div className={'mol-acc-item' + (active === i ? ' active' : '')} key={i}>
            <div className="acc-item-header" onClick={e => {
              e.preventDefault();

              setActive(i);
            }}>
              {d.header}
            </div>
            <div className="acc-item-body">
              {d.body}
            </div>
          </div>
        );
      })}
    </div>
  );
};